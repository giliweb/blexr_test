<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::prefix('/ajax')->group(function(){
    Route::get('/check-login',         'Auth\LoginController@checkLogin');
    Route::get('/logout',              'Auth\LoginController@logout');
    Route::post('/login',              'Auth\LoginController@login');
    Route::middleware('auth')->group(function(){
        Route::middleware('role:administrator')->group(function(){
            Route::get('/users/{page?}',         'AjaxController@fetchUsers');
            Route::post('/user/create',        'AjaxController@createUser');
            Route::post('/user/{user}',        'AjaxController@saveUser');
            Route::get('/requests',         'AjaxController@fetchRequests');
            Route::post('/request/approve/{user}/{request_id}', 'AjaxController@approveRequest');
            Route::post('/request/disapprove/{user}/{request_id}', 'AjaxController@disapproveRequest');
        });

        Route::get('/user/{user}',         'AjaxController@getUser');
        Route::get('/requests/{user}',         'AjaxController@fetchRequests');
        Route::get('/get-features',        'AjaxController@fetchAvailableFeatures');
        Route::get('/get-roles',        'AjaxController@fetchAvailableRoles');
        Route::get('/get-request-types',        'AjaxController@fetchAvailableRequestTypes');
        Route::get('/get-office-locations',        'AjaxController@fetchAvailableOfficeLocations');
        Route::post('/request/cancel/{user}/{request_id}', 'AjaxController@cancelRequest');
        Route::post('/request/save', 'AjaxController@saveRequest');

    });
});

Route::get('/', 'Controller@index')->name('home');

Auth::routes();