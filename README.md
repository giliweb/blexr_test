#How to use it
- git clone git@bitbucket.org:giliweb/blexr_test.git
- Rename `.env.example` to `.env` 
- Update the following values in `.env` file:
```
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```
- From the command line, run the following commands: 
```
composer install
npm install
npm run dev 
php artisan migrate:refresh --seed
php artisan serve
```
- From your browser open the URL `http://localhost:8000`

##Notes - PLEASE READ
The --seed command will create 100 employees 
and 1 administrator accounts for testing purpose. You can login with any of those.
Employee and administrator accounts have, of course, different permissions.
Username for administrator account is `giliweb.it@gmail.com`. 
For employee accounts the email is generated randomly, you can easily get it from the DB 
or from then application itself under the "Users" tabs.
Password is always `secret` for all of those generated accounts.

For whoever it may concerns: this is just a test project created in the late evening, 
after 8 hrs in the office.

As such, code is not so clean sometimes and some security checks are missing. 

I'm aware of that.

I decided to develop this project as a SPA (single page application) to give a better and faster feedback.
To achieve that, i used VueJS2 and VueRouter.
For the same reason i opted to use a simple ajax login, session based, instead of a "Basic authentication" 
as suggested in the task description.


On top of that, i decided to use the Laravel built-in Events feature to control the outgoing email sent to new users 
and to users whose requests have been updated.
This application relies on MVC pattern and Eloquent ORM for any object described as a model (User, Role, Feature, etc).
Database structure relies on migrations to keep trace of any change to the schema.

####Ideas for further developments:
- Split each component's file into 2 or 3 files, one for the template, one for the script part and one for the style, if necessary
- Split `AjaxController` into several sub-controllers classes
- Dig deeper into the role feature, to allow each user to have more than just one role
- One more view available to the administrator, to add more OfficeLocation records
- Add a popup or some visual feedback for the user to realise if their action was successful or it failed
- Add a proper splash screen during the background loading
- Add a loading animation during asynchronous requests
- Write more and more tests (for the frontend part too, using Laravel Dusk, for example)
- Switch to some fancy graphic instead of this barebone layout


##Basic Tests
On linux/cygwin, run phpunit with:
```
./vendor/bin/phpunit
```
OR windows:
```
php vendor/phpunit/phpunit/phpunit
```
 



