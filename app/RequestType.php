<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RequestType
 * @package App
 * request types available for each user
 */
class RequestType extends Model
{
    public const PENDING = "0";
    public const APPROVED = "1";
    public const NOT_APPROVED = "2";
    public const CANCELED = "3";

    protected $fillable = [];
    protected $hidden = [];

    public function users(){
        return $this->belongsToMany(User::class);
    }

    /**
     * @param $value
     * @return mixed
     * used to automatically unserialize the rules value
     * rules could be also saved as a json object, but i chosen to serialize them to allow it to use custom validation class
     */
    public function getRulesAttribute($value){
        return unserialize($value);
    }

}
