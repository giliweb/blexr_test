<?php

namespace App\Http\Controllers;

use App\Events\UserRegistered;
use App\Events\UserRequestUpdated;
use App\Feature;
use App\OfficeLocation;
use App\RequestType;
use App\Role;
use App\User;
use App\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Class AjaxController
 * @package App\Http\Controllers
 * This controller handles all the ajax requests.
 * In a real project more controllers would have been used instead
 */
class AjaxController extends Controller
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * fetch all the users
     * TODO: paginate the results
     */
    public function fetchUsers(){
        return User::all();
    }

    /**
     * @param User $user
     * @return $this|\Illuminate\Http\JsonResponse
     * get details for a specific user
     */
    public function getUser(User $user){
        if(!Auth::user()->hasRole('administrator') && $user->id !== Auth::user()->id){
            return response()->json('Access forbidden', 403);
        }
        return $user->load('features', 'role');
    }

    /**
     * @param User $user
     * @param Request $request
     * @return User
     * save specific user details
     */
    public function saveUser(User $user, Request $request){
        $features = [];
        foreach($request->post('features') as $feature){
            $features[$feature['id']] = ["status" => "{$feature['pivot']['status']}"];
        }
        $user->features()->sync($features);
        return $user->load('features', 'role');
    }

    /**
     * @param Request $request
     * @return User
     * create a user
     */
    public function createUser(Request $request){
        // generate random password
        $password = str_random(8);
        $user = User::create([
            "name" => $request->post('name'),
            "email" => $request->post('email'),
            "password" => $password,
            "role_id" => $request->post('role')['id']
        ]);

        // save user's details
        $user = $this->saveUser($user, $request);

        // trigger the UserRegistered event
        event(new UserRegistered($user, $password));
        return $user;

    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * fetch all the available features
     */
    public function fetchAvailableFeatures(){
        return Feature::all();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * fetch all the available roles
     */
    public function fetchAvailableRoles(){
        return Role::all();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * fetch all the available request types
     */
    public function fetchAvailableRequestTypes(){
        return RequestType::all();
    }

    /**
     * @param User|NULL $user
     * @return array
     * fetch all the requests for a specific user OR for all the users
     * TODO: add filters server side and pagination
     */
    public function fetchRequests(User $user = NULL){
        if($user instanceof User){ // for a specific user
            return array_map(function($e) use($user){
                $e->name = $user->name;
                unset($e->user);
                return $e;
            }, $user->requests->all());
        } else { // no user is specified, returns all the requests for all the users
            return array_map(function($e){
                $e->name = $e->user->name;
                return $e;
            }, UserRequest::all()->load('user')->all());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Support\MessageBag
     * validate and create a new user request
     */
    public function saveRequest(Request $request){

        $requestType = RequestType::find($request->post('request_id'));
        $validationRules = $requestType->rules;

        $validator = Validator::make($request->all(), $validationRules);

        if($validator->fails()){
            return $validator->errors();
        }

        $user = User::find($request->post('user_id'));
        if(!Auth::user()->hasRole('administrator') && $user->id !== Auth::user()->id){
            return response()->json('Access forbidden', 403);
        }

        $user->requests()->create([
            "request_id" => $requestType->id,
            "from" => $request->post('from'),
            "to" => $request->post('to'),
            "hours" => $request->post('hours'),
            "office_location_id" => $request->post('office_location_id'),
        ]);
        $user->load('requests');
        return $user;
    }

    /**
     * @param User $user
     * @param int $userRequestId
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * approve user request
     */
    public function approveRequest(User $user, int $userRequestId){
        $userRequest = $user->requests()->find($userRequestId);
        $userRequest->update(['status' => RequestType::APPROVED]);
        $userRequest->name = $user->name;
        event(new UserRequestUpdated($user, $userRequest));
        return $userRequest;
    }

    /**
     * @param User $user
     * @param int $userRequestId
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * disapprove user request
     */
    public function disapproveRequest(User $user, int $userRequestId){
        $userRequest = $user->requests()->find($userRequestId);
        $userRequest->update(['status' => RequestType::NOT_APPROVED]);
        $userRequest->name = $user->name;
        event(new UserRequestUpdated($user, $userRequest));
        return $userRequest;
    }

    /**
     * @param User $user
     * @param int $userRequestId
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|null|static|static[]
     * a user canceled his own request
     */
    public function cancelRequest(User $user, int $userRequestId){
        if(!$user->id === Auth::user()->id){
            return response()->json('Access forbidden', 403);
        }
        $userRequest = $user->requests()->find($userRequestId);
        $userRequest->update(['status' => RequestType::CANCELED]);
        $userRequest->name = $user->name;
        return $userRequest;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * fetches all available office locations
     */
    public function fetchAvailableOfficeLocations(){
        return OfficeLocation::all();
    }
}
