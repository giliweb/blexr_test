<?php

namespace App\Http\Middleware;

use Closure;

class CheckRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $role
     * @return mixed
     * this middleware ensures the currently logged in user has the requested $role
     */
    public function handle($request, Closure $next, string $role)
    {
        if (! $request->user()->hasRole($role)) {
            return response()->json('you don’t have permission to access this resource', 403);
        }
        return $next($request);
    }
}
