<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRequest
 * @package App
 * user request class
 */
class UserRequest extends Model
{

    protected $fillable = ['request_id', 'hours', 'from', 'to', 'status', 'office_location_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function location(){
        return $this->hasOne(OfficeLocation::class);
    }
}
