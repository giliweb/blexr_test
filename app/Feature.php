<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Feature
 * @package App
 * features available for each user
 */
class Feature extends Model
{
    public const ACTIVE = 1;
    public const NOT_ACTIVE = 0;


    public function users(){
        return $this->belongsToMany(User::class);
    }
}
