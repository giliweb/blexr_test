<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App
 * basic user class
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function requests(){
        return $this->hasMany(UserRequest::class);
    }

    public function features(){
        return $this->belongsToMany(Feature::class)->withPivot('status')->withTimestamps();
    }

    /**
     * @param string $role
     * @return bool
     * simply checks if $this has specified role
     */
    public function hasRole(string $role){
        return $this->role->name === $role;
    }
}
