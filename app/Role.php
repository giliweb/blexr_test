<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @package App
 * basic role class for users
 */
class Role extends Model
{
    public const ADMINISTRATOR = 1;
    public const EMPLOYEE = 9;

    public function users(){
        return $this->hasMany(User::class);
    }
}
