<?php

namespace App\Events;

use App\User;
use App\UserRequest;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class UserRequestUpdated
 * @package App\Events
 * this event is triggered when an user request status has been changed
 */
class UserRequestUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $userRequest;
    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param $userRequest
     */
    public function __construct(User $user, $userRequest)
    {
        $this->user = $user;
        $this->userRequest = $userRequest;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
