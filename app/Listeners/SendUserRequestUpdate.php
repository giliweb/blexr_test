<?php

namespace App\Listeners;

use App\Events\UserRequestUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

/**
 * Class SendUserRequestUpdate
 * @package App\Listeners
 * this listens for UserRequestUpdated event and send an email to the user whose request has been updated
 */
class SendUserRequestUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRequestUpdated  $event
     * @return void
     */
    public function handle(UserRequestUpdated $event)
    {
        Log::info("Sending email to {$event->user->email}. His/her request's status has been updated");
    }
}
