require('./bootstrap');

import VueMoment from 'vue-moment';
import VueRouter from 'vue-router';
import Vue from 'vue';

Vue.use(VueRouter);
Vue.use(VueMoment);

const splash = Vue.component('splash', require('./components/Splash.vue'));
const login = Vue.component('login', require('./components/Login.vue'));
const dashboard = Vue.component('dashboard', require('./components/Dashboard.vue'));
const mainMenu = Vue.component('mainMenu', require('./components/MainMenu.vue'));
const users = Vue.component('users', require('./components/Users.vue'));
const requests = Vue.component('requests', require('./components/Requests.vue'));
const list = Vue.component('list', require('./components/common/List.vue'));
const user = Vue.component('user', require('./components/User.vue'));
const request = Vue.component('request', require('./components/Request.vue'));
const dropdown = Vue.component('dropdown', require('./components/common/DropDown.vue'));

const app = new Vue({
    el: '#app',
    data(){
        return {
            loggedIn: false,
            loading: false,
            user: null,
            roles: null,
            features: null,
            requestTypes: null,
            officeLocations: null
        }
    },
    router: new VueRouter({
        routes: [
            { path: '/',                                   component: splash,            name: 'splash'                   },
            { path: '/dashboard',                          component: dashboard,         name: 'dashboard'                },
            { path: '/login',                              component: login,             name: 'login'                    },
            { path: '/users',                              component: users,             name: 'users'                    },
            { path: '/requests',                           component: requests,          name: 'requests'                 },
            { path: '/requests/:user_id',                  component: requests,          name: 'requests',    props: true },
            { path: '/users/edit/:user_id',                component: user,              name: 'users_edit',  props: true },
            { path: '/users/new',                          component: user,              name: 'users_new'                },
            { path: '/requests/new/:user_id',              component: request,           name: 'request',     props: true },
            { path: '/requests/edit/:user_id/:request_id', component: request,           name: 'request',     props: true },
        ]
    }),
    beforeMount(){
        let oldRoute = this.$route.path;

        this.$router.push({ name: 'splash' });
        axios.get('/ajax/check-login').then(response => {
            if(!response.data.user){
                this.$router.push({ name: 'login' });
            } else {

                this.fetchCommonData(()=>{
                    this.$router.push({ path: oldRoute });
                    this.loggedIn = true;
                    this.user = response.data.user;
                });
            }
        });
    },
    mounted(){

    },
    methods: {
        logout(){
            location.reload();
        },
        login(userData){
            this.fetchCommonData(()=>{
                this.loggedIn = true;
                this.user = userData.user;
                window.axios.defaults.headers.common['X-CSRF-TOKEN'] = userData.token;
                this.$router.push({ name: 'dashboard' });
            });
        },
        fetchCommonData(callback){
            axios.all([
                axios.get('/ajax/get-roles'),
                axios.get('/ajax/get-features'),
                axios.get('/ajax/get-request-types'),
                axios.get('/ajax/get-office-locations'),
            ]).then(axios.spread((roles, features, requestTypes, officeLocations) => {
                this.roles = roles.data;
                this.features = features.data;
                this.requestTypes = requestTypes.data;
                this.officeLocations = officeLocations.data;
                callback();
            }));
        }
    }
});
