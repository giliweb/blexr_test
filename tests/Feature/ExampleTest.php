<?php

namespace Tests\Feature;

use App\Feature;
use App\OfficeLocation;
use App\RequestType;
use App\Role;
use App\User;
use Tests\TestCase;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * @property Faker faker
 */
class ExampleTest extends TestCase
{

    /**
     * @group skip
     * @param string $role
     * login as any user with that specific role
     */
    public function loginAs(string $role){
        $role = Role::where('name', $role)->first();
        $user = User::where('role_id', $role->id)->first();
        $this->actingAs($user);
    }

    /**
     * Test fetch available features
     *
     * @return void
     */
    public function testGetFeatures()
    {
        $response = $this->get('/ajax/get-features');
        $response->assertStatus(302);
        $this->loginAs('employee');
        $response = $this->get('/ajax/get-features');
        $response->assertStatus(200);
        $this->assertJson($response->getContent());
    }

    /**
     * test user creation
     */
    public function testCreateUser(){
        // map the features pivot structure to look like the one from the actual application
        $features = array_map(function($e){
            $e = [
                "id" => $e->id,
                "pivot" => [
                    "status" => "0",
                ]
            ];
            return $e;
        }, Feature::all()->all());
        $user = factory(User::class)->raw(); // using user factory to generate unique email and name
        $user['features'] = $features;
        $user['role']['id'] = Role::EMPLOYEE;

        $response = $this->postJson('/ajax/user/create', $user);
        $response->assertStatus(401);
        $this->loginAs('employee');
        $response = $this->postJson('/ajax/user/create', $user);
        $response->assertStatus(403);
        $this->loginAs('administrator');
        $response = $this->postJson('/ajax/user/create', $user);
        $response->assertStatus(201);
    }

    /**
     * get 50 random users and update them
     */
    public function testUpdateUser(){
        $users = User::all()->shuffle()->slice(0, 50);
        $users->each(function($user){
            // map the features pivot structure to look like the one from the actual application
            $features = array_map(function($e){
                $e = [
                    "id" => $e->id,
                    "pivot" => [
                        "status" => mt_rand(0, 10) > 5 ? "1" : "0",
                    ]
                ];
                return $e;
            }, Feature::all()->all());

            $response = $this->postJson('/ajax/user/' . $user->id, ["features" => $features]);
            $response->assertStatus(401);
        });

        $this->loginAs('employee');
        $users->each(function($user){
            // map the features pivot structure to look like the one from the actual application
            $features = array_map(function($e){
                $e = [
                    "id" => $e->id,
                    "pivot" => [
                        "status" => mt_rand(0, 10) > 5 ? "1" : "0",
                    ]
                ];
                return $e;
            }, Feature::all()->all());

            $response = $this->postJson('/ajax/user/' . $user->id, ["features" => $features]);
            $response->assertStatus(403);
        });

        $this->loginAs('administrator');
        $users->each(function($user){
            // map the features pivot structure to look like the one from the actual application
            $features = array_map(function($e){
                $e = [
                    "id" => $e->id,
                    "pivot" => [
                        "status" => mt_rand(0, 10) > 5 ? "1" : "0",
                    ]
                ];
                return $e;
            }, Feature::all()->all());

            $response = $this->postJson('/ajax/user/' . $user->id, ["features" => $features]);
            $response->assertStatus(200);
        });
    }

    /**
     * get 50 random users and create requests for them
     */
    public function testCreateRequest(){
        $users = User::all()->shuffle()->slice(0, 50);
        $users->each(function($user){
            // sickness
            $request = [
                "request_id" => RequestType::where('name', 'sickness')->first()->id,
                "from" => (new \DateTime('today 8am'))->format('Y-m-d H:i:s'),
                "to" => (new \DateTime('+9 days'))->format('Y-m-d H:i:s'),
                "user_id" => $user->id
            ];
            $response = $this->postJson('/ajax/request/save', $request);
            $response->assertStatus(401);
        });

        $this->loginAs('administrator');
        $users->each(function($user){
            // sickness
            $request = [
                "request_id" => RequestType::where('name', 'sickness')->first()->id,
                "from" => (new \DateTime('today 8am'))->format('Y-m-d H:i:s'),
                "to" => (new \DateTime('+9 days'))->format('Y-m-d H:i:s'),
                "user_id" => $user->id
            ];
            $response = $this->postJson('/ajax/request/save', $request);
            $response->assertStatus(200);
        });

        $users->each(function($user){
            // work from home or different office
            $request = [
                "request_id" => RequestType::where('name', 'work from another place')->first()->id,
                "from" => (new \DateTime('today 8am'))->format('Y-m-d H:i:s'),
                "hours" => mt_rand(2, 40),
                "user_id" => $user->id
            ];
            $response = $this->postJson('/ajax/request/save', $request);

            $this->assertArrayHasKey('office_location_id', json_decode($response->getContent(), true));


            $request = [
                "request_id" => RequestType::where('name', 'work from another place')->first()->id,
                "from" => (new \DateTime('today 8am'))->format('Y-m-d H:i:s'),
                "hours" => mt_rand(2, 40),
                "user_id" => $user->id,
                "office_location_id" => OfficeLocation::all()->first()->id,
            ];
            $response = $this->postJson('/ajax/request/save', $request);

            $response->assertStatus(200);
            $this->assertArrayHasKey('id', json_decode($response->getContent(), true));
        });

    }

}
