<?php

use App\RequestType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('request_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('request_id')->references('id')->on('request_types');
            $table->enum('status', [RequestType::PENDING, RequestType::APPROVED, RequestType::NOT_APPROVED, RequestType::CANCELED])->default(RequestType::PENDING);
            $table->unsignedTinyInteger('hours')->nullable()->default(NULL);
            $table->dateTime('from');
            $table->dateTime('to')->nullable()->default(NULL);
            $table->unsignedInteger('office_location_id')->nullable()->default(NULL);
            $table->foreign('office_location_id')->references('id')->on('office_locations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_requests');
    }
}
