<?php

use App\Feature;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotFeatureUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_user', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('feature_id');
            $table->unique(['user_id', 'feature_id']);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('feature_id')->references('id')->on('features');
            $table->enum('status', [Feature::NOT_ACTIVE, Feature::ACTIVE])->default(Feature::NOT_ACTIVE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_user');
    }
}
