<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequestTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('request_types')->insert([
            [
                'name' => 'work from another place',
                'rules' => serialize([
                    'hours' => 'required|integer|min:1|max:40',
                    'from' => 'required|date|after_or_equal:today +8 hours',
                    'office_location_id' => 'required|exists:office_locations,id'
                ]),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'sickness',
                'rules' => serialize([
                    'from' => 'required|date|after_or_equal:today 8am',
                    'to' => 'required|date|before_or_equal:today +14 days',
                ]),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
        ]);
    }
}
