<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'id' => Role::ADMINISTRATOR,
                'name' => 'administrator',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'id' => Role::EMPLOYEE,
                'name' => 'employee',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
        ]);
    }
}
