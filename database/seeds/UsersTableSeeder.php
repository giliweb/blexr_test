<?php

use App\Feature;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            "name" => "test administrator",
            "email" => "giliweb.it@gmail.com",
            "password" => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
            'remember_token' => str_random(10),
            'role_id' => Role::ADMINISTRATOR,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        factory(User::class, 100)->create()->each(function (User $user) {
            Feature::all()->each(function(Feature $feature) use($user) {
                $user->features()->attach($feature->id);
            });

        });
    }
}
